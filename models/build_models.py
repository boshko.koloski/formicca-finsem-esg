import pandas as pd
import numpy as np
import os
import pickle
from ast import literal_eval

train_reps = {}
test_reps = {}

pref = '../data_esg/reps'

for file in os.listdir(pref):
    path = os.path.join(pref,file)
    out_rep = ''
    if 'csv' in file:
        df = pd.read_csv(path)
        df['vector'] = df['vector'].apply(lambda x: "["+",".join(x[1:-1].split())+"]")
        df['vector'] = df['vector'].apply(lambda x: literal_eval(x))#.to_list())
        out_rep = np.array(df['vector'].to_list())
    else:
        out_rep = np.loadtxt(path, delimiter=',')
    nm = file.split('_')[1].split('.')[0]
    if 'train' in file:
        train_reps[nm] = out_rep 
    else:
        test_reps[nm] = out_rep

print()
pickle.dump((train_reps, test_reps), open('reps.pkl','wb'))
