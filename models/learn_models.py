
import pandas as pd
import numpy as np
import os
import pickle
from ast import literal_eval
from sklearn.model_selection import GridSearchCV, RandomizedSearchCV
from sklearn.linear_model import SGDClassifier
from sklearn.metrics import f1_score

from time import time
def report(results, n_top=3):
    for i in range(1, n_top + 1):
        candidates = np.flatnonzero(results["rank_test_score"] == i)
        for candidate in candidates:
            print("Model with rank: {0}".format(i))
            print(
                "Mean validation score: {0:.3f} (std: {1:.3f})".format(
                    results["mean_test_score"][candidate],
                    results["std_test_score"][candidate],
                )
            )
            print("Parameters: {0}".format(results["params"][candidate]))
            print("")

idxs = pickle.load(open("../data_esg/idxs_splits.pkl","rb"))

df_gold = pd.read_json('../data_esg/train.json')
df_gold['fin_lbl'] = [1 if l[0] == 's' else 0 for l in df_gold['label']]

scikit_generic_final = {
    "loss": ["hinge", "log", "modified_huber"],
    "penalty": ["elasticnet"],
    "power_t": [0.1,0.3,0.5],
    "class_weight": ["balanced"],
    "alpha": [0.01, 0.001, 0.00001],
    "l1_ratio": [0, 0.5, 1]
}

scikit_default = {
    "loss": ["hinge", "log"],
    "penalty": ["elasticnet"],
    "alpha": [0.01, 0.001, 0.0001],
    "l1_ratio": [0.1, 0.5, 0.9]
}

clf = SGDClassifier(loss="hinge", penalty="elasticnet", fit_intercept=True)
grid_search = GridSearchCV(clf, param_grid=scikit_generic_final)
idxs = pickle.load(open("../data_esg/idxs_splits.pkl","rb"))

(train_reps, test_reps) = pickle.load(open('reps.pkl','rb'))
out = []
for rep in train_reps:
    curr_rep = train_reps[rep] 
    train = curr_rep[idxs['train'],:]
    train_l = df_gold['fin_lbl'].iloc[idxs['train']].to_list()
    dev = curr_rep[idxs['dev'],:]
    dev_l = df_gold['fin_lbl'].iloc[idxs['dev']].to_list() #[idxs['dev']]

    train = np.vstack((train, dev))
    train_l = train_l +  dev_l
    
    grid_search.fit(train, train_l)
    start = time()

    print(
        "GridSearchCV took %.2f seconds for %d candidate parameter settings."
        % (time() - start, len(grid_search.cv_results_["params"]))
    )
    report(grid_search.cv_results_)

    outs = []
    for s in ['train','dev','test']:
        test = curr_rep[idxs[s],:]
        test_l =  df_gold['fin_lbl'].iloc[idxs[s]].to_list() #lbls[idxs['test']]
        pred_y = grid_search.predict(test)
        fs = f1_score(test_l, pred_y)
        print(rep,s, fs)
        outs.append((rep, s, pred_y,fs))
    final_test = test_reps[rep]#[idxs[s],:]
    pred_y = grid_search.predict(final_test)
    outs.append((rep, "final_split", pred_y,1.1))

    out.append(outs)

with open('output_learned.pkl', 'wb') as f:
    pickle.dump(out, f)