import pandas as pd;
import numpy as np
df_train = pd.read_json('train.json')
outs = {}
print(df_train)
for t in ['train','dev','test']:
    split_df = pd.read_csv(f'./inside_train/{t}.csv')
    idxs = []
    from tqdm import  tqdm
    for i, r in tqdm(split_df.iterrows(), total = len(split_df)):
        for j,r2 in df_train.iterrows():
            if r['sentence'] == r2['sentence']:
                idxs.append(j)
                break
    #idxs = list(df_train.index[df_train['sentence']==s].values[0] for s in split_df['sentence'])
    #idxs = np.array(idxs)
    print(idxs)
    print(df_train.iloc[idxs])
    outs[t] = idxs

import pickle
pickle.dump(outs, open('idxs_splits.pkl','wb'))
